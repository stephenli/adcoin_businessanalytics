# README #

### What is this repository for? ###

* Adcoin_BusinessAnalytics: A Cloud-based Business Analytics System for Retailing Applications
* You can generate quick insights from your transactional data through this system
* Version 1.0

### How do I get set up? ###

* Summary of set up: install R/Rstudio on your desktop, 
* Configuration: install shiny package in R.
* Dependencies:(R packages) ggplot2; googleVis; stringdist; shinyAce; mailR；shinyIncubator
* Database configuration: N/A
* How to run tests: Please use the file in testfile folder
* Deployment instructions: set up the directory to the root folder, use command: runApp("adcoin_ba") in R/Rstudio
