#a=do.call(rbind,by(record$EXT_PRICE,list(record$CUSTNO),function(x)c(Sum=sum(x),Mean=mean(x),Frequency=length(x))))
#View(a)
#c=aggregate(EXT_PRICE ~ CUSTNO,data = record,function(x)c(a=mean(x),b=length(x)))
#a=do.call(data.frame,by(record$EXT_PRICE,list(record$CUSTNO),function(x)c(Sum=sum(x),Mean=mean(x),Frequency=length(x))))
#View(a)
#b=aggregate(record$EXT_PRICE,list(record$CUSTNO),FUN=c("sum","mean"))
#a=aggregate(record$EXT_PRICE,list(record$CUSTNO),FUN=function(x){c(sum=sum(x),mean=mean(x))})
#fullRecord$ORDER_DATE<-as.Date(fullRecord$ORDER_DATE,format="%m/%d/%Y")
#fullRecord$ORDER_YEAR <- strftime(fullRecord$ORDER_DATE, format="%Y")
#fullRecord<-within(fullRecord,{PROFIT<-EXT_PRICE-EXT_COST})
#head(aggregate(PROFIT~CUSTNO+ORDER_YEAR,fullRecord,sum))
#tapply(fullRecord$PROFIT,fullRecord[,c("CUSTNO","ORDER_YEAR")],sum)

temp=fullRecord
ds=fullRecord
temp$ORDER_DATE<-as.Date(ds$ORDER_DATE,format="%m/%d/%Y")

temp$ORDER_YEAR <- strftime(temp$ORDER_DATE, format="%Y")
temp$PROFIT<-temp$EXT_PRICE-temp$EXT_COST
channelSales<-aggregate(PROFIT~CHANNEL+ORDER_YEAR,temp,FUN=function(x)c(Total=sum(x),Average=mean(x),frequency=length(x)))          

channelSales<-as.data.frame(channelSales)
# Flatten the matrix PROFIT in channelSales
channelSales$frequency=channelSales$PROFIT[,"frequency"]
channelSales$Total=channelSales$PROFIT[,"Total"]
channelSales$Average=channelSales$PROFIT[,"Average"]
channelSales$ORDER_YEAR=as.numeric(channelSales$ORDER_YEAR)   
channelSales<-subset(channelSales,select=-c(PROFIT))
gvisMotionChart(channelSales,
                idvar="CHANNEL", timevar="ORDER_YEAR",
                xvar="frequency", yvar="Total",
                colorvar="CHANNEL", 
                sizevar="Total",
                options=list(width=700, height=600))